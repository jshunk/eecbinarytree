#ifndef BINARYTREE_H
#define BINARYTREE_H

#include "BinaryTreeNode.h"
#include "BinaryTreeIterator.h"

template< typename DataType >
class TBinaryTree
{
public:
										TBinaryTree( DataType oNull );
										~TBinaryTree();
	void								Insert( DataType oNode, i32 iIndex );
	bool								IsPair( const DataType& krData, i32 iIndex ) 
											const;
	TBinaryTreeIterator< DataType >&	GetIterator();
	void								Dereference( TBinaryTreeNode< DataType >* pNode );
	void								CleanHead();

private:
									TBinaryTree() {}

	static const u8					ms_kuNumIterators = 5;

	TBinaryTreeNode< DataType >*	m_pHead;
	DataType						m_oNull;
	TBinaryTreeNode< DataType >*	m_pNext;
	TBinaryTreeIterator< DataType >	m_akIteratorList[ ms_kuNumIterators ];

	friend class TBinaryTreeIterator< DataType >;
};

template< typename DataType >
TBinaryTree< DataType >::TBinaryTree( DataType oNull )
	: m_pHead( NULL )
	, m_oNull( oNull )
	, m_pNext( NULL )
{
	for( u8 i( 0 ); i < ms_kuNumIterators; ++i )
	{
		m_akIteratorList[ i ].SetBinaryTree( *this );
	}
}

template< typename DataType >
TBinaryTree< DataType >::~TBinaryTree()
{
	assert( m_pHead == NULL );
}

template< typename DataType >
void TBinaryTree< DataType >::Insert( DataType oNode, i32 iIndex )
{
	TBinaryTreeNode< DataType >* pInsertNode( 
		new TBinaryTreeNode< DataType >( oNode, iIndex ) );
	if( !m_pHead )
	{
		m_pHead = pInsertNode;
		return;
	}

	m_pHead->Insert( *pInsertNode );
	while( m_pHead->GetParent() )
	{
		m_pHead = m_pHead->GetParent();
	}
}

template< typename DataType >
bool TBinaryTree< DataType >::IsPair( const DataType& krData, i32 iIndex ) const
{
	if( m_pHead )
	{
		TBinaryTreeNode< DataType >* pNext = m_pHead->Find( iIndex );
		while( pNext )
		{
			if( pNext->GetData() == krData )
			{
				return true;
			}
			pNext = pNext->GetNext();
		}
	}
	return false;
}

template< typename DataType >
TBinaryTreeIterator< DataType >& TBinaryTree< DataType >::GetIterator()
{
	for( u8 i( 0 ); i < ms_kuNumIterators; ++i )
	{
		if( !m_akIteratorList[ i ].IsInUse() )
		{
			m_akIteratorList[ i ].Activate();
			return m_akIteratorList[ i ];
		}
	}
	assert( false );
	return m_akIteratorList[ 0 ];
}

template< typename DataType >
void TBinaryTree< DataType >::Dereference( TBinaryTreeNode< DataType >* pNode )
{
	for( u8 i( 0 ); i < ms_kuNumIterators; ++i ) 
	{
		if( m_akIteratorList[ i ].IsInUse() )
		{
			m_akIteratorList[ i ].Dereference( pNode );
		}
	}
}

template< typename DataType >
void TBinaryTree< DataType >::CleanHead()
{
	if( m_pHead && m_pHead->IsMarkedForDeletion() && !m_pHead->GetNext() && 
		!m_pHead->GetLeft() && !m_pHead->GetRight() )
	{
		delete( m_pHead );
		m_pHead = NULL;
	}
}

#endif
