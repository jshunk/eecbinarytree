#ifndef BINARYTREENODE_H
#define BINARYTREENODE_H

#include "assert.h"
#include "../Common/Types.h"
#include "../Common/Utilities.h"

using namespace NUtilities;

template< typename DataType >
class TBinaryTreeNode
{
public:
									TBinaryTreeNode( DataType oData, i32 iIndex );
	u8								Insert( TBinaryTreeNode< DataType >& rChild );
	void							Remove( TBinaryTreeNode< DataType >* pPrevious = NULL );
	void							RemoveIncludingNext();
	DataType						GetData();
	TBinaryTreeNode< DataType >*	Find( i32 iIndex );
	TBinaryTreeNode< DataType >*	Find();
	TBinaryTreeNode< DataType >*	GetNext();
	u8								GetHeight() const;
	TBinaryTreeNode< DataType >*	RotateLeft();
	TBinaryTreeNode< DataType >*	RotateRight();
	TBinaryTreeNode< DataType >*	GetParent();
	i32								GetIndex()	{ return m_iIndex; }
	bool							IsMarkedForDeletion() const;
	TBinaryTreeNode*				GetLeft();
	TBinaryTreeNode*				GetRight();

private:
									TBinaryTreeNode()	{}
	void							SetParent( TBinaryTreeNode< DataType >& rParent );
	void							SetLeft( TBinaryTreeNode< DataType >& rChild );
	void							SetRight( TBinaryTreeNode< DataType >& rChild );
	void							SetNext( TBinaryTreeNode< DataType >& rChild );
	u8								ClearLeft();
	u8								ClearRight();
	void							ClearNext();
	void							ClearParent();
	i32								GetIndex() const;
	bool							IsLeftChild() const;
	void							Replace( TBinaryTreeNode< DataType >& rReplacement );
	TBinaryTreeNode< DataType >*	GetNextHigher();
	void							ReplaceChild( const TBinaryTreeNode< DataType >& rOld,
										TBinaryTreeNode< DataType >& rNew );
	void							ClearChild( 
										const TBinaryTreeNode< DataType >& rChild );

	DataType						m_oData;
	i32								m_iIndex;
	TBinaryTreeNode< DataType >*	m_pParent;
	TBinaryTreeNode< DataType >*	m_pLeft;
	TBinaryTreeNode< DataType >*	m_pRight;
	TBinaryTreeNode< DataType >*	m_pNext;
	bool							m_bMarkedForDeletion;
	u8								m_uHeight;
};

template< typename DataType >
TBinaryTreeNode< DataType >::TBinaryTreeNode( DataType oData, i32 iIndex )
	: m_oData( oData )
	, m_iIndex( iIndex )
	, m_pParent( NULL )
	, m_pLeft( NULL )
	, m_pRight( NULL )
	, m_pNext( NULL )
	, m_bMarkedForDeletion( false )
	, m_uHeight( 0 )
{
}

template< typename DataType >
u8 TBinaryTreeNode< DataType >::Insert( TBinaryTreeNode< DataType >& rChild )
{
	TBinaryTreeNode* pSubParent( NULL );
	if( rChild.GetIndex() > m_iIndex )
	{
		pSubParent = GetRight();
		if( !pSubParent )
		{
			SetRight( rChild );
			return m_uHeight;
		}
	}
	else if( rChild.GetIndex() < m_iIndex )
	{
		pSubParent = GetLeft();
		if( !pSubParent)
		{
			SetLeft( rChild );
			return m_uHeight;
		}
	}
	else 
	{
		TBinaryTreeNode< DataType >* pNode( this );
		while( pNode->GetNext() )
		{
			pNode = pNode->GetNext();
		}

		pNode->SetNext( rChild );
		return m_uHeight;
	}
	m_uHeight = Max< u8 >( pSubParent->Insert( rChild ) + 1, m_uHeight );
	i32 iRightHeight( -1 );
	i32 iLeftHeight( -1 );
	if( m_pRight )
	{
		iRightHeight = m_pRight->GetHeight();
	}
	if( m_pLeft )
	{
		iLeftHeight = m_pLeft->GetHeight();
	}
	if( ( iRightHeight - iLeftHeight ) > 1 )
	{
		RotateLeft();
	}
	else if( ( iRightHeight - iLeftHeight ) < -1 )
	{
		RotateRight();
	}
	return m_uHeight;
}

template< typename DataType >
void TBinaryTreeNode< DataType >::Remove( TBinaryTreeNode< DataType >* pPrevious )
{
	if( !pPrevious && !m_pNext && !m_pParent )
	{
		m_bMarkedForDeletion = true;
	}

	if( pPrevious )
	{
		pPrevious->m_pNext = m_pNext;
	}

	if( m_pNext )
	{
		Replace( *m_pNext );
		return;
	}

	if( m_pParent )
	{
		TBinaryTreeNode< DataType >* pChild;
		if( !GetLeft() && GetRight() )
		{
			pChild = GetRight();
		}
		else if( GetLeft() && !GetRight() )
		{
			pChild = GetLeft();
		}
		else if( GetLeft() && GetRight() )
		{
			pChild = GetNextHigher();
			assert( pChild );
			pChild->RemoveIncludingNext();
			Replace( *pChild );
			return;
		}
		else
		{
			m_pParent->ClearChild( *this );
			return;
		}

		m_pParent->ReplaceChild( *this, *pChild );
	}
}

template< typename DataType >
void TBinaryTreeNode< DataType >::RemoveIncludingNext()
{
	if( m_pParent )
	{
		TBinaryTreeNode< DataType >* pChild;
		if( !GetLeft() && GetRight() )
		{
			pChild = GetRight();
		}
		else if( GetLeft() && !GetRight() )
		{
			pChild = GetLeft();
		}
		else if( GetLeft() && GetRight() )
		{
			pChild = GetNextHigher();
			assert( pChild );
			pChild->RemoveIncludingNext();
			Replace( *pChild );
			return;
		}
		else
		{
			m_pParent->ClearChild( *this );
			return;
		}

		m_pParent->ReplaceChild( *this, *pChild );
	}
}

template< typename DataType >
DataType TBinaryTreeNode< DataType >::GetData()
{
	return m_oData;
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::Find( i32 iIndex )
{
	if( m_iIndex == iIndex )
	{
		if( m_bMarkedForDeletion )
		{
			if( m_pNext )
			{
				return m_pNext->Find( iIndex );
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			return this;
		}
	}

	if( iIndex > m_iIndex )
	{
		if( !m_pRight )
		{
			return NULL;
		}
		else
		{
			return m_pRight->Find( iIndex );
		}
	}

	if( !m_pLeft )
	{
		return NULL;
	}
	else
	{
		return m_pLeft->Find( iIndex );
	}
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::Find()
{
	if( m_bMarkedForDeletion )
	{
		if( m_pNext )
		{
			return m_pNext->Find();
		}
	}
	else
	{
		return this;
	}

	if( m_pRight )
	{
		return m_pRight->Find();
	}

	if( m_pLeft )
	{
		return m_pLeft->Find();
	}

	return NULL;
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::GetNext()
{
	return m_pNext;
}

template< typename DataType >
u8 TBinaryTreeNode< DataType >::GetHeight() const
{
	return m_uHeight;
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::RotateLeft()
{
	assert( GetRight() );
	TBinaryTreeNode< DataType >* pParent( m_pParent );
	m_pParent = GetRight();
	ClearRight();
	if( m_pParent->GetLeft() )
	{
		SetRight( *( m_pParent->GetLeft() ) );
		m_pParent->ClearLeft();
	}

	m_pParent->SetLeft( *this );
	if( pParent )
	{
		pParent->ReplaceChild( *this, *m_pParent );
	}
	else
	{
		m_pParent->ClearParent();
	}

	return m_pParent;
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::RotateRight()
{
	assert( GetLeft() );
	TBinaryTreeNode< DataType >* pParent( m_pParent );
	m_pParent = GetLeft();
	ClearLeft();
	if( m_pParent->GetRight() )
	{
		SetLeft( *( m_pParent->GetRight() ) );
		m_pParent->ClearRight();
	}
	else
	{
		ClearLeft();
	}

	m_pParent->SetRight( *this );
	if( pParent )
	{
		pParent->ReplaceChild( *this, *m_pParent );
	}
	else
	{
		m_pParent->ClearParent();
	}

	return m_pParent;
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::GetParent()
{
	return m_pParent;
}

template< typename DataType >
bool TBinaryTreeNode< DataType >::IsMarkedForDeletion() const
{
	return m_bMarkedForDeletion;
}

template< typename DataType >
void TBinaryTreeNode< DataType >::SetParent( TBinaryTreeNode< DataType >& rParent )
{
	m_pParent = &rParent;
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::GetLeft()
{
	return m_pLeft;
}

template< typename DataType >
void TBinaryTreeNode< DataType >::SetLeft( TBinaryTreeNode< DataType >& rChild )
{
	assert( !m_pLeft );
	m_pLeft = &rChild;
	rChild.SetParent( *this );
	m_uHeight = Max< u8 >( rChild.GetHeight() + 1, m_uHeight );
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::GetRight()
{
	return m_pRight;
}

template< typename DataType >
void TBinaryTreeNode< DataType >::SetRight( TBinaryTreeNode< DataType >& rChild )
{
	assert( !m_pRight );
	m_pRight = &rChild;
	rChild.SetParent( *this );
	m_uHeight = Max< u8 >( m_pRight->GetHeight() + 1, m_uHeight );
}

template< typename DataType >
void TBinaryTreeNode< DataType >::SetNext( TBinaryTreeNode< DataType >& rChild )
{
	assert( !m_pNext );
	m_pNext = &rChild;
}

template< typename DataType >
u8 TBinaryTreeNode< DataType >::ClearLeft()
{
	m_pLeft = NULL;
	if( m_pRight )
	{
		m_uHeight = m_pRight->GetHeight() + 1;
	}
	else
	{
		m_uHeight = 0;
	}

	return m_uHeight;
}

template< typename DataType >
u8 TBinaryTreeNode< DataType >::ClearRight()
{
	m_pRight = NULL;
	if( m_pLeft )
	{
		m_uHeight = m_pLeft->GetHeight() + 1;
	}
	else
	{
		m_uHeight = 0;
	}

	return m_uHeight;
}

template< typename DataType >
void TBinaryTreeNode< DataType >::ClearNext()
{
	m_pNext = NULL;
}

template< typename DataType >
void TBinaryTreeNode< DataType >::ClearParent()
{
	m_pParent = NULL;
}

template< typename DataType >
i32 TBinaryTreeNode< DataType >::GetIndex() const
{
	return m_iIndex;
}

template< typename DataType >
void TBinaryTreeNode< DataType >::Replace( TBinaryTreeNode< DataType >& rReplacement )
{
	if( m_pLeft )
	{
		rReplacement.ClearLeft();
		rReplacement.SetLeft( *GetLeft() );
	}

	if( m_pRight )
	{
		m_pNext->ClearRight();
		m_pNext->SetRight( *GetRight() );
	}

	if( m_pParent )
	{
		bool bIsLeftChild( m_pParent->GetLeft() == this );
		if( bIsLeftChild )
		{
			m_pParent->ClearLeft();
			m_pParent->SetLeft( rReplacement );
		}
		else
		{
			assert( m_pParent->GetRight() == this );
			m_pParent->ClearRight();
			m_pParent->SetRight( rReplacement );
		}
	}
}

template< typename DataType >
TBinaryTreeNode< DataType >* TBinaryTreeNode< DataType >::GetNextHigher()
{
	TBinaryTreeNode< DataType >* pNextHigher( GetRight() );
	if( !pNextHigher )
	{
		return NULL;
	}

	while( pNextHigher->GetLeft() )
	{
		pNextHigher = pNextHigher->GetLeft();
	}

	return pNextHigher;
}

template< typename DataType >
void TBinaryTreeNode< DataType >::ReplaceChild( const TBinaryTreeNode< DataType >& krOld,
									TBinaryTreeNode< DataType >& rNew )
{
	if( GetLeft() == &krOld )
	{
		ClearLeft();
		SetLeft( rNew );
		return;
	}

	assert( GetRight() == &krOld );
	ClearRight();
	SetRight( rNew );
}

template< typename DataType >
void TBinaryTreeNode< DataType >::ClearChild( 
									const TBinaryTreeNode< DataType >& krChild )
{
	if( GetLeft() == &krChild )
	{
		ClearLeft();
		return;
	}

	assert( GetRight() == &krChild );
	ClearRight();
}

#endif
