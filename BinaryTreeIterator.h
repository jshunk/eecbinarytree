#ifndef BINARYTREEITERATOR_H
#define BINARYTREEITERATOR_H

#include "BinaryTree.h"

template< typename DataType >
class TBinaryTree;

template< typename DataType >
class TBinaryTreeIterator
{
public:
				TBinaryTreeIterator();
	void		SetBinaryTree( TBinaryTree< DataType >& rBinaryTree );
	bool		IsInUse() const;
	void		Activate();
	void		Deactivate();
	DataType	Find( i32 iIndex );
	DataType	FindNext();
	bool		Remove( i32 iIndex );
	bool		RemoveNext();
	bool		RemovePair( const DataType& rData, i32 iIndex );
	bool		RemovePairNext();
	DataType	Take( i32 iIndex );
	DataType	TakeNext();
	DataType	Take();
	void		Dereference( TBinaryTreeNode< DataType >* pNode );

private:
	TBinaryTree< DataType >*		m_pBinaryTree;
	bool							m_bInUse;
	TBinaryTreeNode< DataType >*	m_pNext;
};

template< typename DataType >
TBinaryTreeIterator< DataType >::TBinaryTreeIterator()
: m_pBinaryTree( NULL )
, m_bInUse( false )
, m_pNext( NULL )
{
}

template< typename DataType >
void TBinaryTreeIterator< DataType >::SetBinaryTree( TBinaryTree< DataType >& rBinaryTree )
{
	m_pBinaryTree = &rBinaryTree;
}

template< typename DataType >
bool TBinaryTreeIterator< DataType >::IsInUse() const
{
	return m_bInUse;
}

template< typename DataType >
void TBinaryTreeIterator< DataType >::Activate()
{
	m_bInUse = true;
}

template< typename DataType >
void TBinaryTreeIterator< DataType >::Deactivate()
{
	m_bInUse = false;
	m_pNext = NULL;
}

template< typename DataType >
DataType TBinaryTreeIterator< DataType >::Find( i32 iIndex )
{
	assert( m_pBinaryTree );
	TBinaryTreeNode< DataType >* pFoundNode( NULL );
	if( m_pBinaryTree->m_pHead )
	{
		pFoundNode = m_pBinaryTree->m_pHead->Find( iIndex );
	}

	if( pFoundNode )
	{
		m_pNext = pFoundNode->GetNext();
		return pFoundNode->GetData();
	}
	else
	{
		return m_pBinaryTree->m_oNull;
	}
}

template< typename DataType >
DataType TBinaryTreeIterator< DataType>::FindNext()
{
	if( m_pNext )
	{
		DataType oNext( m_pNext->GetData() );
		m_pNext = m_pNext->GetNext();
		return oNext;
	}

	return m_pBinaryTree->m_oNull;
}

template< typename DataType >
bool TBinaryTreeIterator< DataType >::Remove( i32 iIndex )
{
	assert( m_pBinaryTree );
	if( m_pBinaryTree->m_pHead )
	{
		TBinaryTreeNode< DataType >* pRemove( m_pBinaryTree->m_pHead->Find( iIndex ) );
		if( pRemove )
		{
			m_pNext = pRemove->GetNext();
			pRemove->Remove();
			delete( pRemove );
			return true;
		}
	}

	m_pNext = NULL;
	return false;
}

template< typename DataType >
bool TBinaryTreeIterator< DataType>::RemoveNext()
{
	if( m_pNext )
	{
		TBinaryTreeNode< DataType >* pRemove( m_pNext );
		m_pNext = m_pNext->GetNext();
		pRemove->Remove();
		delete( pRemove );
		return true;
	}

	return false;
}

template< typename DataType >
bool TBinaryTreeIterator< DataType >::RemovePair( const DataType& krData, i32 iIndex )
{
	assert( m_pBinaryTree );
	if( m_pBinaryTree->m_pHead )
	{
		TBinaryTreeNode< DataType >* pPrevious( NULL );
		m_pNext = m_pBinaryTree->m_pHead->Find( iIndex );
		while( m_pNext && m_pNext->GetData() != krData )
		{
			pPrevious = m_pNext;
			m_pNext = m_pNext->GetNext();
		}
		if( m_pNext )
		{
			TBinaryTreeNode< DataType >* pRemove( m_pNext );
			m_pNext = pRemove->GetNext();
			m_pBinaryTree->Dereference( pRemove );
			pRemove->Remove( pPrevious );
			if( m_pBinaryTree->m_pHead != pRemove )
			{
				delete( pRemove );
				m_pBinaryTree->CleanHead();
			}
			while( m_pNext && m_pNext->GetData() != krData )
			{
				m_pNext = m_pNext->GetNext();
			}
			return true;
		}
	}
	return false;
}

template< typename DataType >
bool TBinaryTreeIterator< DataType >::RemovePairNext()
{
	if( m_pNext )
	{
		DataType oData( m_pNext->GetData() );
		TBinaryTreeNode< DataType >* pRemove( m_pNext );
		m_pNext = pRemove->GetNext();
		pRemove->Remove();
		delete( pRemove );
		while( m_pNext && m_pNext->GetData() != oData )
		{
			m_pNext = m_pNext->GetNext();
		}

		return true;
	}

	return false;
}

template< typename DataType >
DataType TBinaryTreeIterator< DataType >::Take( i32 iIndex )
{
	assert( m_pBinaryTree );

	// Find a matching node, remove it, update the next pointer, and update the iterators:
	if( m_pBinaryTree->m_pHead )
	{
		// Find a matching node:
		m_pNext = m_pBinaryTree->m_pHead->Find( iIndex );
		if( m_pNext )
		{
			// Get the node to remove and update the next pointer:
			DataType oReturn( m_pNext->GetData() );
			TBinaryTreeNode< DataType >* pRemove( m_pNext );
			m_pNext = pRemove->GetNext();

			// Make sure no iterators point to the removed node:
			for( u8 i( 0 ); i < TBinaryTree< DataType >::ms_kuNumIterators; ++i )
			{
				if( m_pBinaryTree->m_akIteratorList[ i ].m_pNext == pRemove )
				{
					m_pBinaryTree->m_akIteratorList[ i ].m_pNext = m_pNext;
				}
			}

			// Remove and delete the node:
			pRemove->Remove();
			if( pRemove == m_pBinaryTree->m_pHead )
			{
				m_pBinaryTree->m_pHead = NULL;
			}
			delete( pRemove );
			return oReturn;
		}
	}
	return m_pBinaryTree->m_oNull;
}

template< typename DataType >
DataType TBinaryTreeIterator< DataType >::TakeNext()
{
	if( m_pNext )
	{
		DataType oNext( m_pNext->GetData() );
		TBinaryTreeNode< DataType >* pRemove( m_pNext );
		m_pNext = pRemove->GetNext();
		pRemove->Remove();
		delete( pRemove );
		return oNext;
	}

	return m_pBinaryTree->m_oNull;
}

template< typename DataType >
DataType TBinaryTreeIterator< DataType >::Take()
{
	assert( m_pBinaryTree );

	// Find a matching node, remove it, update the next pointer, and update the iterators:
	if( m_pBinaryTree->m_pHead )
	{
		// Find a matching node:
		m_pNext = m_pBinaryTree->m_pHead->Find();
		if( m_pNext )
		{
			// Get the node to remove and update the next pointer:
			DataType oReturn( m_pNext->GetData() );
			TBinaryTreeNode< DataType >* pRemove( m_pNext );
			m_pNext = pRemove->GetNext();

			// Make sure no iterators point to the removed node:
			for( u8 i( 0 ); i < TBinaryTree< DataType >::ms_kuNumIterators; ++i )
			{
				if( m_pBinaryTree->m_akIteratorList[ i ].m_pNext == pRemove )
				{
					m_pBinaryTree->m_akIteratorList[ i ].m_pNext = m_pNext;
				}
			}

			// Remove and delete the node:
			pRemove->Remove();
			if( pRemove == m_pBinaryTree->m_pHead )
			{
				m_pBinaryTree->m_pHead = NULL;
			}
			delete( pRemove );
			return oReturn;
		}
	}
	return m_pBinaryTree->m_oNull;
}

template< typename DataType >
void TBinaryTreeIterator< DataType >::Dereference( TBinaryTreeNode< DataType >* pNode )
{
	assert( pNode );
	if( m_pNext == pNode )
	{
		m_pNext = pNode->GetNext();
	}
}

#endif
